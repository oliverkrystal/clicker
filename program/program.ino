//Clicker
//sean zellers aka @oliverkrystal


//Declare Pin connections
int led = 0;
int clickDisable = 1;
int thresholdPin = 20;
int delayPin = 19 ;
int photocell = 21;

bool lightOn = false;

void setup() {
	Serial.begin(9600); //I might want to be able to see what's going on in case things don't work as expected live.
	pinMode(led, OUTPUT);
	pinMode(clickDisable, INPUT);

}

void loop() {
	//Poll initial constants, might be dynamic later but I think best practice for now to make static
	//later
	int clickDisablePos = digitalRead(clickDisable);
	int threshhold = (1024-analogRead(thresholdPin));
	unsigned long delayAmount = (1024-analogRead(delayPin));
	int photocellVal = analogRead(photocell);

	Serial.print("Threshold: ");
	Serial.println(threshhold);
	Serial.print("Delay Amount: ");
	Serial.println(delayAmount);
	Serial.print("Photocell value: ");
	Serial.println(photocellVal);

	if (photocellVal > threshhold){
		Serial.println("Click wanted");
		if (!clickDisablePos)
			Keyboard.print("n");
		digitalWrite(led, HIGH);
		unsigned long holdTime = (millis() + (delayAmount+25));
		while (holdTime > millis()){
			delay(50);
		}
		digitalWrite(led, LOW);
	}
	//may need this later, not sure but probably
	delay(250);

}


