# Clicker

![Front View](IMG_2800.jpg) ![Back View](IMG_2801.jpg)
This is a clicker sort of thing for advancing slides (or something) on your computer during a presentation.  It was conceived because I didn't want to carry a laser pointer and a slide remote and I had the stuff to do it.  Its still somewhat cheaper (I think) but its usability has yet to be determined.

## Parts
+ Teensy 2.0 (https://www.sparkfun.com/products/12765)
+ 0 to 1k ohm (LDR https://www.sparkfun.com/products/9088)
+ 2 potentiometer (https://www.sparkfun.com/products/9939)
+ An on/off switch
+ LED
+ resistor
+ Laser pointer

## Schematics
![Schematics](IMG_2802.jpg)

## Things included
The io-test directory contains a script that can test all of the inputs and outputs and lets you see if you hooked everything up correctly to the teensy 2.0.  You'll need to open a serial port to the teensy as it will ping back telling you feedback from the sensors.  IMG_2802.jpg is the schematics diagram.  I used a protoboard from radioshack to do hookups.

## Usage
* Before plugging the clicker in, flip the switch to stop the clicker from sending "n" key strikes.  The green LED will turn on and off whenever a "click" is sensed.  Using the left potentiometer turn all the way to the left, and then adjust to the right until the LED is no longer lit.  You can be a little fuzzy - just so long as the led is no longer lit. This adjusts for ambient light.
* If you find that the slide is advancing to many times every time you hit the LDR with a laser, adjust the right potentiometer to the right to increase the delay after the slide advances the first time.
* If you need a different key (for whatever reason), you will need to recompile the software after editing the relevant line. (Line 13)