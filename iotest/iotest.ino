int photocell = 21;
int led = 0;
int potentiometerLeft = 20;
int potentiometerRight = 19 ;
int clickDisable = 1;
bool lightOn = false;


void setup() {
	Serial.begin(9600);

	pinMode(led, OUTPUT);
	pinMode(clickDisable, INPUT);

}

void loop() {

	Serial.println(analogRead(photocell));
	Serial.println(1024-(analogRead(potentiometerLeft)));
	Serial.println(1024-(analogRead(potentiometerRight)));
	if (digitalRead(clickDisable) == LOW)
		Serial.println("button off");
	else
		Serial.println("button on");
	if (lightOn == true){
		digitalWrite(led, LOW);
		lightOn = false;
		Serial.print("led off\n");
	} else {
		digitalWrite(led, HIGH);
		lightOn = true;
		Serial.print("led on\n");
	}

	delay(500);
}